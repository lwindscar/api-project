import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FILE_MODEL_NAME, File, FileModel } from './files.schema';
import { BaseService } from '../shared/dao';

@Injectable()
export class FilesService extends BaseService<File> {
  constructor(
    @InjectModel(FILE_MODEL_NAME) private readonly fileModel: FileModel,
  ) {
    super(fileModel);
  }
}
