import mongoose, { BaseDocument, BaseModel } from '../shared/mongo';

export const FileSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  uid: {
    type: String,
    required: true,
    unique: true,
  },
  mimetype: String,
  extname: String,
  size: Number,
  path: {
    type: String,
    required: true,
  },
});

FileSchema.virtual('url').get(function() {
  return 'http://localhost:3000/' + this.path;
});

export interface File extends BaseDocument {
  readonly name: string;
  readonly uid: string;
  readonly mimetype: string;
  readonly extname: string;
  readonly size: number;
  readonly path: string;
  readonly url: string;
}

export const FILE_MODEL_NAME = 'File';

export interface FileModel extends BaseModel<File> {}
