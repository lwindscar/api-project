import { Controller, Post, Body, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import * as path from 'path';
import { PlainObject, MulterFile } from '../shared/types';
import { FilesService } from './files.service';

@Controller('files')
export class FilesController {
  constructor(
    private readonly filesService: FilesService,
  ) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: MulterFile, @Body() body: PlainObject) {
    const { filename, originalname, size, mimetype, path: filePath } = file;
    const { ext: extname, name: uid } = path.parse(filename);
    return this.filesService.create({
      name: originalname,
      uid,
      extname,
      mimetype,
      size,
      path: filePath,
    });
  }
}
