import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule, MulterModuleOptions } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as path from 'path';
import * as uuid from 'uuid/v4';
import { ConfigService } from '../config/config.service';
import { FilesService } from './files.service';
import { FilesController } from './files.controller';
import { FILE_MODEL_NAME, FileSchema } from './files.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: FILE_MODEL_NAME, schema: FileSchema }]),
    MulterModule.registerAsync({
      useFactory: (config: ConfigService): MulterModuleOptions => {
        const storage = diskStorage({
          destination: config.get('MULTER_DEST'),
          filename(req, file, cb) {
            const extname = path.extname(file.originalname);
            cb(null, uuid() + extname);
          },
        });
        return { storage };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [FilesService],
  controllers: [FilesController],
  exports: [FilesService],
})
export class FilesModule {}
