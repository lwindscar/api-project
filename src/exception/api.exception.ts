import { HttpException, HttpStatus } from '@nestjs/common';
import { PlainObject } from '../shared/types';

export class ApiException extends HttpException {
  constructor(code: number, message: string | PlainObject) {
    const response = { code, message };
    super(response, HttpStatus.BAD_REQUEST);
  }
}
