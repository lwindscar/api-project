import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  RequestTimeoutException,
  HttpException,
  InternalServerErrorException,
} from '@nestjs/common';
import { Observable, throwError, TimeoutError } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';

@Injectable()
export class TimeoutInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      timeout(15000),
      catchError(err => {
        const exception =
          err instanceof TimeoutError
            ? new RequestTimeoutException()
            : err instanceof HttpException
            ? err
            : new InternalServerErrorException();
        return throwError(exception);
      }),
    );
  }
}
