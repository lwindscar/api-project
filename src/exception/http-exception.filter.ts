import { ExceptionFilter, HttpException, ArgumentsHost, Catch, Logger } from '@nestjs/common';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const statusCode = exception.getStatus();
    const errorResponse: any = exception.getResponse();
    const error = {
      statusCode,
      message: errorResponse.message || errorResponse.error,
      code: errorResponse.code,
    };
    const request = ctx.getRequest();
    const { url, method, headers, query, body } = request;
    const [baseUrl] = url.split('?');
    const message = `[HttpExceptionFilter] ${method} ${baseUrl} ${statusCode} ${JSON.stringify(error)}
      headers: ${JSON.stringify(headers)}
      query: ${JSON.stringify(query)}
      body: ${JSON.stringify(body)}`;
    Logger.error(message);
    /* tslint:disable */
    console.log(exception);
    /* tslint:enable */
    response
      .status(statusCode)
      .json({ error });
  }
}
