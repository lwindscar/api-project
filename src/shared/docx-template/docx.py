from os import path
from sys import argv
from uuid import uuid4
from win32com.client import constants, DispatchEx
import pythoncom

dirname = path.dirname(__file__)
tplPath = argv[1]
reportName = str(uuid4()) + '_report.pdf'
reportPath = path.abspath(path.join(dirname, '../../../uploads', reportName))


def convertWordToPdf(docxPath, pdfPath):
    pythoncom.CoInitialize()
    w = DispatchEx("Word.Application")
    w.Visible = 0
    w.DisplayAlerts = 0

    try:
        doc = w.Documents.Open(docxPath, ReadOnly=1)
        doc.ExportAsFixedFormat(
            pdfPath,
            constants.wdExportFormatPDF,
            Item=constants.wdExportDocumentWithMarkup,
            CreateBookmarks=constants.wdExportCreateHeadingBookmarks
        )
        doc.Close()
    except Exception as e:
        print('exception')
    finally:
        w.Quit(constants.wdDoNotSaveChanges)
        del(word)
        pythoncom.CoUninitialize()


convertWordToPdf(tplPath, reportPath)

print(reportName)
