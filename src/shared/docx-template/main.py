from docxtpl import DocxTemplate
from os import path
from sys import argv
# from docopt import docopt
import json
from uuid import uuid4

# print(__file__, '----filePath----')

dirname = path.dirname(__file__)
# print(dirname, '----dirname-----')

tplPath = argv[1]
data = json.loads(argv[2])
# print(data)

# print('\n'.join(argv))

reportName = str(uuid4()) + '_report.docx'
reportPath = path.abspath(path.join(dirname, '../../../uploads', reportName))

tpl = DocxTemplate(tplPath)
tpl.render(data)
tpl.save(reportPath)

print(reportName)
