import dotenv from 'dotenv';
import fs from 'fs';
import path from 'path';
import { isProduction } from './helper';
import logger from './logger';

let dotenvFilePath = '.env.example';
if (isProduction) {
  const isDotenvFileExisted = fs.existsSync(path.resolve(__dirname, '../.env'));
  if (isDotenvFileExisted) {
    dotenvFilePath = '.env';
  } else {
    logger.error('.env file is not existed');
    process.exit(1);
  }
}
dotenv.config({ path: dotenvFilePath });

export const getEnvItem = (key: string) => {
  const value = process.env[key];
  if (!value) {
    logger.error(`Environment variable ${key} is not defined`);
    return process.exit(1);
  }
  return value;
};

export const checkEnvConfig = (envConfig, rules = []) => {
  rules.forEach(({ key, message = '', type = 'warning' }) => {
    const envKeys = Array.isArray(key) ? key : [key];
    const unSpecifiedKeys = envKeys.filter(k => !envConfig[k]);
    if (unSpecifiedKeys.length > 0) {
      const msg = `env ${unSpecifiedKeys.join(', ')} not specified, ${message}`;
      if (type === 'error') {
        logger.error(msg);
        process.exit(1);
      } else {
        logger.warn(msg);
      }
    }
  });
};
