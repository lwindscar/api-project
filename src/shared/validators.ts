import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import mongoose from './mongo';
import { UserSchema, USER_MODEL_NAME } from '../users/users.schema';

const userModel = mongoose.model(USER_MODEL_NAME, UserSchema); // userModel doesn't work

export function IsSameWith(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      name: 'IsSameWith',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = (args.object as any)[relatedPropertyName];
          return value === relatedValue;
        },
        defaultMessage() {
          return '$property is not same with $constraint1';
        },
      },
    });
  };
}

@ValidatorConstraint({ async: true })
export class IsUserAlreadyExistConstraint implements ValidatorConstraintInterface {
  async validate(username: any, args: ValidationArguments) {
    return userModel.findOne({ username }).then(user => {
      return !user;
    });
  }

  defaultMessage() {
    return '$property is already existed';
  }
}

export function IsUserAlreadyExist(validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsUserAlreadyExistConstraint,
    });
  };
}
