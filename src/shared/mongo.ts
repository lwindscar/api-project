import * as mongoose from 'mongoose';
// import * as mongoosePaginate from 'mongoose-paginate';
// import * as mongooseCursorPaginate from 'mongoose-cursor-paginate';
import { PlainObject } from './types';

const globalSchemaPlugin = (schema: mongoose.Schema) => {
  schema.set('timestamps', true);
  schema.set('toJSON', {
    getters: true,
    virtuals: true,
    versionKey: false,
    transform(doc: mongoose.Document, ret: object) {
      Reflect.deleteProperty(ret, '_id');
      return ret;
    },
    ...schema.get('toJSON'),
  });
};

mongoose.plugin(globalSchemaPlugin);

export default mongoose;

export interface BaseDocument extends mongoose.Document {
  readonly id: string;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export interface BaseModel<T extends BaseDocument> extends mongoose.Model<T> {
}

export interface PaginateOptions {
  select?: PlainObject | string;
  sort?: PlainObject | string;
  populate?: PlainObject[] | string[] | PlainObject | string;
  lean?: boolean;
  leanWithId?: boolean;
  offset?: number;
  page?: number;
  limit?: number;
}

export interface PaginateResult<T> {
  docs: T[];
  total: number;
  limit: number;
  page?: number;
  pages?: number;
  offset?: number;
}

export interface PaginateModel<T extends BaseDocument> extends BaseModel<T> {
  paginate(
    query?: PlainObject,
    options?: PaginateOptions,
    callback?: (err: any, result: PaginateResult<T>) => void,
  ): Promise<PaginateResult<T>>;
}

export interface CursorPaginateOptions {
  query?: PlainObject;
  limit?: number;
  fields?: PlainObject;
  paginatedField?: string;
  next?: string;
  previous?: string;
  sortAscending?: boolean;
}

export interface CursorPaginateResult<T> {
  results: T[];
  previous: string;
  hasPrevious: boolean;
  next: string;
  hasNext: boolean;
}

export interface CursorPaginateModel<T extends BaseDocument> extends BaseModel<T> {
  paginate(
    options?: CursorPaginateOptions,
    callback?: (err: any, result: CursorPaginateResult<T>) => void,
  ): Promise<CursorPaginateResult<T>>;
}

export interface PaginateList<T> {
  data: T[];
  current: number;
  pageSize: number;
  total: number;
}

export interface CursorPaginateList<T> {
  data: T[];
  previous: string;
  hasPrevious: boolean;
  next: string;
  hasNext: boolean;
}

// static methods
// schema.statics.pagination = async function(page = 1, limit = 10) {
//   const total = await this.countDocuments();
//   const query = this instanceof mongoose.Query ? this : this.find();
//   const numberPage = typeof page === 'number' ? page : Number.parseInt(page, 10);
//   const numberLimit = typeof limit === 'number' ? limit : Number.parseInt(limit, 10);

//   const data = await query
//     .skip((numberPage - 1) * numberLimit)
//     .limit(numberLimit)
//     .exec();
//   return {
//     data,
//     pagination: {
//       page,
//       limit,
//       total,
//     },
//   };
// }

// schema.statics.cursorPagination = async function(after, limit = 10, sort = 'id') {
//   const query = this instanceof mongoose.Query ? this : this.find();
//   const numberLimit = typeof limit === 'number' ? limit : Number.parseInt(limit, 10);
//   const sortField = sort === 'id' ? '_id' : sort;
//   if (after) {
//     // @ts-ignore
//     query.gt(sortField, after);
//   }
//   const docs = await query
//     .sort(sortField)
//     .limit(numberLimit + 1)
//     .exec();
//   const docBefore = docs[numberLimit];
//   const before = docBefore ? docBefore[sort] : null;
//   const data = docs.slice(0, numberLimit);
//   return {
//     data,
//     pagination: {
//       limit,
//       sort,
//       after,
//       before,
//     },
//   };
// }

// export const createSchema = (schema: mongoose.Schema) => {
//   return new mongoose.Schema({
//     ...schema,
//     __v: {
//       type: Number,
//       select: false,
//     },
//   }, {
//     timestamps: true,
//     toJSON: {
//       getters: true,
//       virtuals: false,
//       versionKey: false,
//     },
//   });
// };
