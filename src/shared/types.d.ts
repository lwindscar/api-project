export interface PlainObject {
  [key: string]: any;
}

export type CallBack = (error?: Error, ...args: any[]) => void;

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export type Mutable<T> = { -readonly [P in keyof T]: T[P] };

export type MutableRequired<T> = { -readonly [P in keyof T]-?: T[P] };

export interface MulterFile {
  /** Field name specified in the form */
  fieldname: string;
  /** Name of the file on the user's computer */
  originalname: string;
  /** Encoding type of the file */
  encoding: string;
  /** Mime type of the file */
  mimetype: string;
  /** Size of the file in bytes */
  size: number;
  /** The folder to which the file has been saved (DiskStorage) */
  destination: string;
  /** The name of the file within the destination (DiskStorage) */
  filename: string;
  /** Location of the uploaded file (DiskStorage) */
  path: string;
  /** A Buffer of the entire file (MemoryStorage) */
  buffer: Buffer;
}