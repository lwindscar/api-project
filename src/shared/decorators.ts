import { createParamDecorator, SetMetadata } from '@nestjs/common';

export const CurrentUser = createParamDecorator((data, req) => {
  return req.user;
});

// export const Logger = createParamDecorator((data, req) => {
//   return req.logger;
// });

export const Roles = (...roles: string[]) => SetMetadata('roles', roles);
