import * as bcrypt from 'bcryptjs';
import * as fs from 'fs';
import * as split from 'split2';
import * as miss from 'mississippi';

export const isProduction = process.env.NODE_ENV && process.env.NODE_ENV.toLowerCase() === 'production';

export const hashPwd = (value: string) => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(value, salt);
  return hash;
};

export const comparePwd = bcrypt.compareSync;

// Function to convert an Uint8Array to a string
export const uint8arrayToString = (data: Uint8Array) => String.fromCharCode.apply(null, data);

export const readFileByLine = (path: string, promise: any, count = 1) => new Promise((resolve, reject) => {
  miss.pipe(
    fs.createReadStream(path), // one url per line
    split(),
    miss.parallel(count, (data, cb) => {
      Promise.resolve(promise(data))
        .then(() => cb(null))
        .catch(err => cb(err));
    }),
    err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    },
  );
});

export const delay = (ms: number = 0, payload?: any) => new Promise(resolve => setTimeout(() => resolve(payload), ms));
