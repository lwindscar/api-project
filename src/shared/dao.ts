// import { Injectable } from '@nestjs/common';
// import { InjectModel } from '@nestjs/mongoose';
import {
  BaseDocument,
  BaseModel,
  PaginateModel,
  PaginateList,
  PaginateOptions,
  CursorPaginateModel,
  CursorPaginateList,
  CursorPaginateOptions,
} from './mongo';
import { PlainObject } from './types';

export class BaseService<T extends BaseDocument> {
  constructor(
    private readonly model: BaseModel<T>,
  ) {}

  async create(payload: PlainObject): Promise<T> {
    const document = new this.model(payload);
    await document.save();
    return document.toJSON();
  }

  async findAll(condition?: PlainObject): Promise<T[]> {
    const documents = await this.model.find(condition);
    return documents.map(document => document.toJSON());
  }

  async findOneById(id: string): Promise<T> {
    const document = await this.model.findById(id);
    if (document) {
      return document.toJSON();
    } else {
      return null;
    }
  }

  async updateOneById(id: string, payload: PlainObject): Promise<T> {
    const document = await this.model.findByIdAndUpdate(id, payload, { new: true });
    if (document) {
      return document.toJSON();
    } else {
      return null;
    }
  }

  async deleteOneById(id: string): Promise<T> {
    const document = await this.model.findByIdAndDelete(id);
    if (document) {
      return document.toJSON();
    } else {
      return null;
    }
  }
}

export class PaginateService<T extends BaseDocument> extends BaseService<T> {
  constructor(
    private readonly paginateModel: PaginateModel<T>,
  ) {
    super(paginateModel);
  }

  async findList(condition?: PlainObject, options?: PaginateOptions): Promise<PaginateList<T>> {
    const list = await this.paginateModel.paginate(condition, options);
    return {
      data: list.docs.map(document => document.toJSON()),
      current: list.page,
      pageSize: list.limit,
      total: list.total,
    };
  }
}

export class CursorPaginateService<T extends BaseDocument> extends BaseService<T> {
  constructor(
    private readonly paginateModel: CursorPaginateModel<T>,
  ) {
    super(paginateModel);
  }

  async findList(options?: CursorPaginateOptions): Promise<CursorPaginateList<T>> {
    const list = await this.paginateModel.paginate(options);
    return {
      data: list.results.map(document => document.toJSON()),
      previous: list.previous,
      hasPrevious: list.hasNext,
      next: list.next,
      hasNext: list.hasNext,
    };
  }
}
