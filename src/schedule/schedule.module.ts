import { Module } from '@nestjs/common';
import { ScheduleService } from './schedule.service';

@Module({
  // @ts-ignore
  providers: [ScheduleService],
  exports: [ScheduleService],
})
export class ScheduleModule {}
