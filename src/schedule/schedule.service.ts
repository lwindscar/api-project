import { Injectable, Logger } from '@nestjs/common';
import { NestSchedule, Cron, Interval } from 'nest-schedule';

@Injectable()
export class ScheduleService extends NestSchedule {

  @Cron('0 0 2 * *', {
    startTime: new Date(),
    endTime: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
    tz: 'Asia/Shanghai',
    enable: false,
  })
  async cronJob() {
    Logger.log('executing cron job');
  }

  @Interval(5000, { enable: false })
  intervalJob() {
    Logger.log('executing interval job');
  }
}
