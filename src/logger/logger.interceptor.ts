import { Injectable, NestInterceptor, ExecutionContext, CallHandler, Logger } from '@nestjs/common';
// import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggerInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler) {
    const controller = context.getClass().name;
    const handler = context.getHandler().name;
    const req = context.switchToHttp().getRequest();
    const { url, method, headers, query, body } = req;
    const [baseUrl] = url.split('?');
    const reqMessage = `[${controller}/${handler}] ${method} ${baseUrl}
      headers: ${JSON.stringify(headers)}
      query: ${JSON.stringify(query)}
      body: ${JSON.stringify(body)}`;
    Logger.log(reqMessage, null, false);

    const timestamp = Date.now();
    return next
      .handle()
      .pipe(
        tap(() => {
          const ms = Date.now() - timestamp;
          const resMessage = `[${controller}/${handler}] ${method} ${baseUrl} responseTime: ${ms} ms`;
          Logger.log(resMessage, null, false);
        }),
      );
  }
}
