import { NextFunction, Request, Response } from 'express';
import { Injectable, Logger, NestMiddleware } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    const { baseUrl, method, headers, query, body } = req;
    const message = `${method} ${baseUrl}
      headers: ${JSON.stringify(headers)}
      query: ${JSON.stringify(query)}
      body: ${JSON.stringify(body)}
    `;
    Logger.log(message);
    next();
  }
}
