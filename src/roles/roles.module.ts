import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RolesService } from './roles.service';
import { RolesController } from './roles.controller';
import { ROLE_MODEL_NAME, RoleSchema } from './roles.schema';
import { ROLE_USER_MODEL_NAME, RoleUserSchema } from './role_user.schema';
import { ROLE_PERMISSION_MODEL_NAME, RolePermissionSchema } from './role_permission.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ROLE_MODEL_NAME, schema: RoleSchema },
      { name: ROLE_USER_MODEL_NAME, schema: RoleUserSchema },
      { name: ROLE_PERMISSION_MODEL_NAME, schema: RolePermissionSchema },
    ]),
  ],
  providers: [RolesService],
  controllers: [RolesController],
  exports: [RolesService],
})
export class RolesModule {}
