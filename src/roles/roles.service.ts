import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { difference, groupBy, mapValues, map } from 'lodash';
import { ROLE_MODEL_NAME, RoleModel, Role } from './roles.schema';
import { ROLE_USER_MODEL_NAME, RoleUserModel } from './role_user.schema';
import { ROLE_PERMISSION_MODEL_NAME, RolePermissionModel } from './role_permission.schema';
import { BaseService } from '../shared/dao';

@Injectable()
export class RolesService extends BaseService<Role> {
  constructor(
    @InjectModel(ROLE_MODEL_NAME) private readonly roleModel: RoleModel,
    @InjectModel(ROLE_USER_MODEL_NAME) private readonly roleUserModel: RoleUserModel,
    @InjectModel(ROLE_PERMISSION_MODEL_NAME) private readonly rolePermissionModel: RolePermissionModel,
  ) {
    super(roleModel);
  }

  async findPermissionsByRole(roleId: string) {
    const joins = await this.rolePermissionModel.find({ role: roleId });
    return joins.map(item => item.permission.toString());
  }

  async updateRolePermissions(roleId: string, permissions: string[]) {
    const currentPermissions = await this.findPermissionsByRole(roleId);
    const permissionsToAdd = difference(permissions, currentPermissions);
    if (permissionsToAdd.length > 0) {
      const docs = permissionsToAdd.map(pid => ({ permission: pid, role: roleId }));
      await this.rolePermissionModel.insertMany(docs);
    }
    const permissionsToRemove = difference(currentPermissions, permissions);
    if (permissionsToRemove.length > 0) {
      await this.rolePermissionModel.deleteMany({
        role: roleId,
        permission: { $in: permissionsToRemove },
      });
    }
  }

  async findRolePermissions(roleIds: string[]) {
    const joins = await this.rolePermissionModel
      .find({ role: { $in: roleIds } })
      .populate('permission');
    let permissionsGroupByRole: any = groupBy(joins, 'role');
    permissionsGroupByRole = mapValues(permissionsGroupByRole, items => map(items, 'permission'));
    return permissionsGroupByRole;
  }

  async updateUserRoles(userId: string, roles: string[]) {
    const joins = await this.roleUserModel.find({ user: userId });
    const currentRoles = joins.map(item => item.role.toString());
    const rolesToAdd = difference(roles, currentRoles);
    if (rolesToAdd.length > 0) {
      const docs = rolesToAdd.map(rid => ({ role: rid, user: userId }));
      await this.roleUserModel.insertMany(docs);
    }
    const rolesToRemove = difference(currentRoles, roles);
    if (rolesToRemove.length > 0) {
      await this.roleUserModel.deleteMany({
        user: userId,
        role: { $in: rolesToRemove },
      });
    }
  }

  async findUserRoles(userIds: string[]) {
    const joins = await this.roleUserModel
      .find({ user: { $in: userIds } })
      .populate('role');
    let rolesGroupByUser: any = groupBy(joins, 'user');
    rolesGroupByUser = mapValues(rolesGroupByUser, items => map(items, 'role'));
    return rolesGroupByUser;
  }
}
