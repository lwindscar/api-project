import {
  IsString,
  IsMobilePhone,
  MinLength,
  IsOptional,
  IsMongoId,
  IsInt,
  IsPositive,
  IsBoolean,
  IsAlphanumeric,
  IsArray,
} from 'class-validator';
import Sanitizer from 'class-sanitizer';
import { Transform, Expose } from 'class-transformer';
import { toUpper, snakeCase, flow, uniq, isArray } from 'lodash';

export class CreateRoleDto {

  @IsAlphanumeric()
  @Transform(flow([snakeCase, toUpper]))
  readonly code: string;

  @IsString()
  readonly name: string;

  @IsString()
  @IsOptional()
  readonly description?: string;
}

export class EditRoleDto {

  @IsString()
  @IsOptional()
  readonly name?: string;

  @IsString()
  @IsOptional()
  readonly description?: string;

  @IsMongoId({ each: true })
  @IsArray()
  @IsOptional()
  @Transform(v => (isArray(v) ? uniq(v) : v))
  readonly permissions?: string[];
}
