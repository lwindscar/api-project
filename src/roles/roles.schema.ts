import mongoose, { BaseDocument, BaseModel } from '../shared/mongo';

export const RoleSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  description: String,
});

export interface Role extends BaseDocument {
  readonly code: string;
  readonly name: string;
  readonly description: string;
}

export const ROLE_MODEL_NAME = 'Role';

export interface RoleModel extends BaseModel<Role> {}
