import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { get } from 'lodash';
import { RolesService } from './roles.service';
import { CreateRoleDto, EditRoleDto } from './roles.dto';

@Controller('roles')
@UseGuards(AuthGuard())
export class RolesController {
  constructor(
    private readonly rolesService: RolesService,
  ) {}

  @Post()
  createRole(@Body() createRoleDto: CreateRoleDto) {
    return this.rolesService.create(createRoleDto);
  }

  @Get()
  async getRoles() {
    const roles: any[] = await this.rolesService.findAll();
    if (roles.length === 0) {
      return [];
    }
    const roleIds = roles.map(r => r.id);
    const rolePermissionsMap = await this.rolesService.findRolePermissions(roleIds);
    return roles.map(r => ({
      ...r,
      permissions: get(rolePermissionsMap, r.id, []),
    }));
  }

  @Put(':id')
  async editRole(@Param('id') id: string, @Body() editRoleDto: EditRoleDto) {
    const { permissions, ...payload } = editRoleDto;
    const role = await this.rolesService.updateOneById(id, payload);
    if (!role) {
      return null;
    }
    if (permissions) {
      await this.rolesService.updateRolePermissions(role.id, permissions);
    }
    return role;
  }

  @Delete(':id')
  @HttpCode(204)
  removeRole(@Param('id') id: string) {
    return this.rolesService.deleteOneById(id);
  }
}
