import mongoose, { BaseDocument, BaseModel } from '../shared/mongo';
import { USER_MODEL_NAME } from '../users/users.schema';
import { ROLE_MODEL_NAME } from './roles.schema';

export const RoleUserSchema = new mongoose.Schema({
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: ROLE_MODEL_NAME,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: USER_MODEL_NAME,
    required: true,
  },
});

export interface RoleUser extends BaseDocument {
  readonly role: string;
  readonly user: string;
}

export const ROLE_USER_MODEL_NAME = 'Role_User';

export interface RoleUserModel extends BaseModel<RoleUser> {}
