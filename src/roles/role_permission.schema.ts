import mongoose, { BaseDocument, BaseModel } from '../shared/mongo';
import { PERMISSION_MODEL_NAME } from '../permissions/permissions.schema';
import { ROLE_MODEL_NAME } from './roles.schema';

export const RolePermissionSchema = new mongoose.Schema({
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: ROLE_MODEL_NAME,
    required: true,
  },
  permission: {
    type: mongoose.Schema.Types.ObjectId,
    ref: PERMISSION_MODEL_NAME,
    required: true,
  },
});

export interface RolePermission extends BaseDocument {
  readonly role: string;
  readonly permission: string;
}

export const ROLE_PERMISSION_MODEL_NAME = 'Role_Permission';

export interface RolePermissionModel extends BaseModel<RolePermission> {}
