import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
  ) {}

  canActivate(context: ExecutionContext) {
    const roles = this.reflector.get<string[]>('roles', context.getHandler()) || this.reflector.get<string[]>('roles', context.getClass());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const currentUser = request.user;
    if (!currentUser) {
      return false;
    }
    const currentUserRoles = currentUser.roles.map(r => r.code);
    const hasRole = currentUserRoles.some(r => roles.includes(r));
    return hasRole;
  }
}
