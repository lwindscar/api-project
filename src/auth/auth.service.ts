import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { User } from '../users/users.schema';
import { UserOrigin, JWTPayload } from './auth.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  authenticate(
    user: User,
    type: UserOrigin = UserOrigin.app,
  ): string {
    const payload: JWTPayload = {
      id: user.id,
      username: user.username,
      type,
    };
    return this.jwtService.sign(payload);
  }

  async validate(payload: JWTPayload) {
    const user = await this.usersService.findOneById(payload.id);
    return user;
  }
}
