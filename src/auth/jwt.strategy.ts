import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { get, flatten, uniqBy } from 'lodash';
import config from '../config/config.service';
import { AuthService } from './auth.service';
import { JWTPayload } from './auth.interface';
import { RolesService } from '../roles/roles.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly authService: AuthService,
    private readonly rolesService: RolesService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: config.get('TOKEN_SECRET'),
    });
  }

  async validate(payload: JWTPayload) {
    const user = await this.authService.validate(payload);
    if (!user) {
      throw new UnauthorizedException();
    }
    const userRolesMap = await this.rolesService.findUserRoles([user.id]);
    const roles = get(userRolesMap, user.id, []);
    if (roles.length === 0) {
      return { ...user, roles: [], permissions: [] };
    }
    const roleIds = roles.map(r => r.id);
    const rolePermissionsMap = await this.rolesService.findRolePermissions(roleIds);
    const permissions = uniqBy(flatten(Object.values(rolePermissionsMap)), 'id');
    return { ...user, roles, permissions };
  }
}
