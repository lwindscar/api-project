import { Body, Controller, Post, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { LoginDto } from './auth.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  async login(@Body() payload: LoginDto) {
    const user = await this.usersService.findOneByUserName(payload.username);
    if (!user) {
      throw new UnauthorizedException();
    }
    const isValid = user.comparePassword(payload.password);
    if (!isValid) {
      throw new UnauthorizedException();
    }
    const token = this.authService.authenticate(user);
    return {
      token,
      user: user.toJSON(),
    };
  }
}
