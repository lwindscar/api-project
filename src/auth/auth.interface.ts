export enum UserOrigin {
  admin = 'ADMIN',
  app = 'APP',
  weChat = 'WE_CHAT',
}

export interface JWTPayload {
  id: string;
  username: string;
  type: UserOrigin;
}
