import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PermissionsController } from './permissions.controller';
import { PermissionsService } from './permissions.service';
import { PERMISSION_MODEL_NAME, PermissionSchema } from './permissions.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: PERMISSION_MODEL_NAME, schema: PermissionSchema }]),
  ],
  controllers: [PermissionsController],
  providers: [PermissionsService],
  exports: [PermissionsService],
})
export class PermissionsModule {}
