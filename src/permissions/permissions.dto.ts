import {
  IsString,
  IsOptional,
  IsAlphanumeric,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { toUpper, snakeCase, flow } from 'lodash';

export class CreatePermissionDto {

  @IsAlphanumeric()
  @Transform(flow([snakeCase, toUpper]))
  readonly code: string;

  @IsString()
  readonly name: string;
}

export class EditPermissionDto {

  @IsString()
  @IsOptional()
  readonly name?: string;
}
