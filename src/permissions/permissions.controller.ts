import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PermissionsService } from './permissions.service';
import { CreatePermissionDto, EditPermissionDto } from './permissions.dto';

@Controller('permissions')
@UseGuards(AuthGuard())
export class PermissionsController {
  constructor(
    private readonly permissionsService: PermissionsService,
  ) {}

  @Post()
  createPermission(@Body() createPermissionDto: CreatePermissionDto) {
    return this.permissionsService.create(createPermissionDto);
  }

  @Get()
  getPermissions() {
    return this.permissionsService.findAll();
  }

  @Put(':id')
  editPermission(@Param('id') id: string, @Body() editPermissionDto: EditPermissionDto) {
    return this.permissionsService.updateOneById(id, editPermissionDto);
  }

  @Delete(':id')
  @HttpCode(204)
  removePermission(@Param('id') id: string) {
    return this.permissionsService.deleteOneById(id);
  }
}
