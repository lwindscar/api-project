import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PERMISSION_MODEL_NAME, Permission, PermissionModel } from './permissions.schema';
import { BaseService } from '../shared/dao';

@Injectable()
export class PermissionsService extends BaseService<Permission> {
  constructor(
    @InjectModel(PERMISSION_MODEL_NAME) private readonly permissionModel: PermissionModel,
  ) {
    super(permissionModel);
  }
}
