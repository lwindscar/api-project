import mongoose, { BaseDocument, BaseModel } from '../shared/mongo';

export const PermissionSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
});

export interface Permission extends BaseDocument {
  readonly code: string;
  readonly name: string;
}

export const PERMISSION_MODEL_NAME = 'Permission';

export interface PermissionModel extends BaseModel<Permission> {}
