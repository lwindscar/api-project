import mongoose, { BaseDocument, CursorPaginateModel } from '../shared/mongo';
import * as mongooseCursorPaginate from 'mongoose-cursor-paginate';
import { USER_MODEL_NAME } from '../users/users.schema';

export const MessageSchema = new mongoose.Schema({
  content: String,
  read: {
    type: Boolean,
    default: false,
  },
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: USER_MODEL_NAME,
    required: true,
  },
  recipient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: USER_MODEL_NAME,
    required: true,
  },
});

MessageSchema.plugin(mongooseCursorPaginate);

export interface Message extends BaseDocument {
  readonly content: string;
  readonly read: boolean;
  readonly sender: string;
  readonly recipient: string;
}

export const MESSAGE_MODEL_NAME = 'Message';

export interface MessageModel extends CursorPaginateModel<Message> {}
