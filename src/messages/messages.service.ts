import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CursorPaginateService } from '../shared/dao';
import { MESSAGE_MODEL_NAME, MessageModel, Message } from './messages.schema';

@Injectable()
export class MessagesService extends CursorPaginateService<Message> {
  constructor(
    @InjectModel(MESSAGE_MODEL_NAME) private readonly messageModel: MessageModel,
  ) {
    super(messageModel);
  }
}
