import { Injectable } from '@nestjs/common';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import * as Joi from 'joi';
import { isProduction } from '../shared/helper';

export interface EnvConfig {
  [prop: string]: string;
}

@Injectable()
export class ConfigService {
  public readonly isProduction: boolean = isProduction;
  private readonly envConfig: EnvConfig;

  constructor() {
    const filePath = this.isProduction ? '.env' : '.example.env';
    const config = dotenv.parse(fs.readFileSync(filePath));
    this.envConfig = this.validateInput(config);
  }

  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      // NODE_ENV: Joi.string()
      //   .valid(['development', 'production', 'test'])
      //   .default('development'),
      PORT: Joi.number().default(3000),
      MONGO_URI: Joi.string().required(),
      API_AUTH_ENABLED: Joi.boolean().required(),
      TOKEN_SECRET: Joi.string().required(),
      MULTER_DEST: Joi.string().default('uploads'),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}

export default new ConfigService();
