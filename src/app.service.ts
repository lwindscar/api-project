import { Injectable, Logger } from '@nestjs/common';
// import { PythonShell } from 'python-shell';
import * as path from 'path';
import { spawn } from 'child_process';
import { uint8arrayToString } from './shared/helper';
import { PlainObject } from './shared/types';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello!';
  }

  async wordToPDF(tplPath: string) {
    const script = path.resolve(__dirname, 'shared/docx-template/docx.py');
    const command = 'pipenv';
    const args = ['run', 'python', '-u', script, tplPath];
    return new Promise((resolve, reject) => {
      const scriptExecution = spawn(command, args);

      // Handle normal output
      scriptExecution.stdout.on('data', (data) => {
        const res = uint8arrayToString(data);
        Logger.log(`Python script response: ${res}`);
        resolve(res);
      });

      // Handle error output
      scriptExecution.stderr.on('data', (data) => {
        // As said before, convert the Uint8Array to a readable string.
        const err = uint8arrayToString(data);
        Logger.error(`Python script error: ${err}`);
        reject(err);
      });

      scriptExecution.on('exit', (code) => {
        Logger.log('Python script process quit with code : ' + code);
      });
    });
  }

  async createReport(tplPath: string, payload: PlainObject) {
    const script = path.resolve(__dirname, 'shared/docx-template/main.py');
    const command = 'pipenv';
    const args = ['run', 'python', '-u', script, tplPath, JSON.stringify(payload)];
    return new Promise((resolve, reject) => {
      const scriptExecution = spawn(command, args);

      // Handle normal output
      scriptExecution.stdout.on('data', (data) => {
        const res = uint8arrayToString(data);
        Logger.log(`Python script response: ${res}`);
        resolve(res);
      });

      // Handle error output
      scriptExecution.stderr.on('data', (data) => {
        // As said before, convert the Uint8Array to a readable string.
        const err = uint8arrayToString(data);
        Logger.error(`Python script error: ${err}`);
        reject(err);
      });

      scriptExecution.on('exit', (code) => {
        Logger.log('Python script process quit with code : ' + code);
      });
    });
  }

  // async exportDocument(str) {
  //   const script = path.resolve(__dirname, 'shared/docx-template/main.py');
  //   const tplPath = path.resolve(__dirname, 'shared/docx-template/templates/word2016_tpl.docx');
  //   const data = {
  //     name: 'Demo',
  //     number: '001',
  //     age: 22,
  //     texts: ['Jack', 'Tom'],
  //     test_space: str,
  //   };
  //   const options = {
  //     pythonPath: 'python3',
  //     pythonOptions: ['-u'], // get print results in real-time
  //     args: [tplPath, JSON.stringify(data)],
  //   };
  //   return new Promise((resolve, reject) => {
  //     PythonShell.run(script, options, (err, res) => {
  //       if (err) {
  //         return reject(err);
  //       }
  //       resolve(res);
  //     });
  //   });
  // }
}
