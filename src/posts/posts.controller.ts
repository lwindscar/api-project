import {
  Controller,
  Post,
  UseGuards,
  Body,
  Get,
  Query,
  Param,
  Put,
  Delete,
  HttpCode,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { PostsService } from './posts.service';
import { CreatePostDto, EditPostDto } from './posts.dto';
import { CurrentUser } from '../shared/decorators';
import { User } from '../users/users.schema';
import { GetUsersDto } from 'src/users/users.dto';

@Controller('posts')
@UseGuards(AuthGuard())
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Post()
  createPost(@Body() body: CreatePostDto, @CurrentUser() user: User) {
    const authorId = user.id;
    return this.postsService.create({ ...body, author: authorId });
  }

  @Get()
  getPosts(@Query() query: GetUsersDto) {
    const { limit = 10, page = 1 } = query;
    return this.postsService.findList(
      {},
      {
        populate: 'author',
        page,
        limit,
      },
    );
  }

  @Get(':id')
  getPost(@Param('id') id: string) {
    return this.postsService.findOneById(id);
  }

  @Put(':id')
  editPost(@Param('id') id: string, @Body() body: EditPostDto) {
    return this.postsService.updateOneById(id, body);
  }

  @Delete(':id')
  @HttpCode(204)
  removePost(@Param('id') id: string) {
    return this.postsService.deleteOneById(id);
  }
}
