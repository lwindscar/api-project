import { Injectable } from '@nestjs/common';
import { InjectModel, InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { POST_MODEL_NAME, PostModel, Post } from './posts.schema';
import { PaginateService } from '../shared/dao';
import { PlainObject } from '../shared/types';

@Injectable()
export class PostsService extends PaginateService<Post> {
  constructor(
    @InjectModel(POST_MODEL_NAME) private readonly postModel: PostModel,
    @InjectConnection() private readonly connection: Connection,
  ) {
    super(postModel);
  }

  // MongoDB currently only supports transactions on replica sets
  // async transactionUpdateOneById(id: string, payload: PlainObject) {
  //   const session = await this.connection.startSession();
  //   try {
  //     session.startTransaction();
  //     const post = await this.postModel.findByIdAndUpdate(id, payload).session(session);
  //     session.commitTransaction();
  //     return post;
  //   } catch (e) {
  //     session.abortTransaction();
  //   }
  // }
}
