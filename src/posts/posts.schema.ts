import mongoose, { BaseDocument, PaginateModel } from '../shared/mongo';
import * as mongoosePaginate from 'mongoose-paginate';
import { USER_MODEL_NAME } from '../users/users.schema';

export const PostSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  content: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: USER_MODEL_NAME,
    required: true,
  },
  slug: String,
});

PostSchema.plugin(mongoosePaginate);

export interface Post extends BaseDocument {
  readonly title: string;
  readonly content: string;
  readonly author: string;
}

export const POST_MODEL_NAME = 'Post';

export interface PostModel extends PaginateModel<Post> {}
