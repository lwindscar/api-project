import {
  IsString,
  IsMobilePhone,
  MinLength,
  IsOptional,
  IsMongoId,
  IsInt,
  IsPositive,
  IsBoolean,
  IsArray,
} from 'class-validator';
import Sanitizer from 'class-sanitizer';
import { Transform, Expose } from 'class-transformer';

export class CreatePostDto {

  @IsString()
  readonly title: string;

  @IsString()
  @IsOptional()
  readonly content?: string;
}

export class EditPostDto {

  @IsString()
  @IsOptional()
  readonly title?: string;

  @IsString()
  @IsOptional()
  readonly content?: string;
}

export class GetPostsDto {

  @IsPositive()
  @IsInt()
  @IsOptional()
  @Transform(Sanitizer.toInt)
  readonly current?: number;

  @Expose()
  get page() {
    return this.current;
  }

  @IsPositive()
  @IsInt()
  @IsOptional()
  @Transform(Sanitizer.toInt)
  readonly pageSize?: number;

  @Expose()
  get limit() {
    return this.pageSize;
  }

  @IsString()
  @IsOptional()
  readonly title?: string;

  @IsString()
  @IsOptional()
  readonly content?: string;
}
