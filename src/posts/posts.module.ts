import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { POST_MODEL_NAME, PostSchema } from './posts.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: POST_MODEL_NAME, schema: PostSchema }]),
  ],
  providers: [PostsService],
  controllers: [PostsController],
})
export class PostsModule {}
