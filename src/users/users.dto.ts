import {
  IsString,
  IsMobilePhone,
  MinLength,
  IsOptional,
  IsMongoId,
  IsInt,
  IsPositive,
  IsBoolean,
  IsArray,
} from 'class-validator';
import Sanitizer from 'class-sanitizer';
import { Transform, Expose } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { uniq, isArray } from 'lodash';
import { IsSameWith } from '../shared/validators';

export class CreateUserDto {

  @ApiModelProperty()
  @IsString()
  readonly name: string;

  @ApiModelPropertyOptional()
  @IsMobilePhone('zh-CN')
  @IsOptional()
  readonly mobile?: string;

  @ApiModelProperty()
  // @IsUserAlreadyExist()
  @MinLength(3)
  @IsString()
  readonly username: string;

  @ApiModelProperty()
  @MinLength(8)
  @IsString()
  readonly password: string;

  @ApiModelProperty()
  @IsSameWith('password')
  @MinLength(8)
  @IsString()
  readonly repassword: string;
}

export class ParamDto {

  @ApiModelProperty()
  @IsMongoId()
  readonly id: string;
}

export class EditUserDto {

  @ApiModelPropertyOptional()
  @IsString()
  @IsOptional()
  readonly name?: string;

  @ApiModelPropertyOptional()
  @IsMobilePhone('zh-CN')
  @IsOptional()
  readonly mobile?: string;

  @ApiModelPropertyOptional()
  @IsMongoId({ each: true })
  @IsArray()
  @IsOptional()
  @Transform(v => (isArray(v) ? uniq(v) : v))
  readonly roles?: string[];
}

export class GetUsersDto {

  @ApiModelPropertyOptional()
  @IsPositive()
  @IsInt()
  @IsOptional()
  @Transform(Sanitizer.toInt)
  readonly current?: number;

  @Expose()
  get page() {
    return this.current;
  }

  @ApiModelPropertyOptional()
  @IsPositive()
  @IsInt()
  @IsOptional()
  @Transform(Sanitizer.toInt)
  readonly pageSize?: number;

  @Expose()
  get limit() {
    return this.pageSize;
  }

  @ApiModelPropertyOptional()
  @IsBoolean()
  @IsOptional()
  @Transform(Sanitizer.toBoolean)
  readonly enable?: boolean;
}
