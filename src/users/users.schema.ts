import mongoose, { BaseDocument, PaginateModel } from '../shared/mongo';
import * as mongoosePaginate from 'mongoose-paginate';
import { hashPwd, comparePwd } from '../shared/helper';
import { Mutable, CallBack } from '../shared/types';

export const UserSchema = new mongoose.Schema({
  name: String,
  mobile: String,
  username: {
    type: String,
    required: true,
    index: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    // select: false,
  },
  avatar: String,
}, {
  toJSON: {
    transform(doc: mongoose.Document, ret: object) {
      Reflect.deleteProperty(ret, '_id');
      Reflect.deleteProperty(ret, 'password');
      return ret;
    },
  },
});

export interface User extends BaseDocument {
  readonly name: string;
  readonly mobile: string;
  readonly username: string;
  readonly password: string;
  readonly avatar: string;

  comparePassword(value: string): boolean;
}

export const USER_MODEL_NAME = 'User';

export interface UserModel extends PaginateModel<User> {}

UserSchema.pre('save', function(this: Mutable<User>, next: CallBack) {
  if (this.isModified('password')) {
    this.password = hashPwd(this.password);
    return next();
  }
  next();
});

UserSchema.methods.comparePassword = function(this: User, value: string) {
  return comparePwd(value, this.password);
};

UserSchema.plugin(mongoosePaginate);
