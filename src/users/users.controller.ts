import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  HttpCode,
  NotFoundException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';
import { ApiBearerAuth } from '@nestjs/swagger';
import { get } from 'lodash';
import { CreateUserDto, ParamDto, EditUserDto, GetUsersDto } from './users.dto';
import { UsersService } from './users.service';
import { CurrentUser, Roles } from '../shared/decorators';
import { RolesService } from '../roles/roles.service';
import { RolesGuard } from '../auth/roles.guard';
import { ApiException } from 'src/exception/api.exception';

@Controller('users')
@ApiBearerAuth()
@UseGuards(AuthGuard(), RolesGuard)
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly rolesService: RolesService,
  ) {}

  @Post()
  async createUser(@Body() body: CreateUserDto) {
    const { username } = body;
    let user = await this.usersService.findOneByUserName(username);
    if (user) {
      throw new ApiException(100, `username ${username} is already occupied`);
    }
    user = await this.usersService.create(body);
    return user;
  }

  @Get()
  @Roles('ADMIN')
  async getUsers(@Req() request: Request, @Query() query: GetUsersDto) {
    const { limit = 10, page = 1 } = query;
    const list: any = await this.usersService.findList({}, { limit, page });
    if (list.total === 0) {
      return list;
    }
    const { data: users } = list;
    const userIds = users.map(u => u.id);
    const userRolesMap = await this.rolesService.findUserRoles(userIds);
    list.data = users.map(u => ({
      ...u,
      roles: get(userRolesMap, u.id, []),
    }));
    return list;
  }

  @Get('current')
  async getCurrentUser(@CurrentUser() user) {
    return user;
  }

  @Get(':id')
  async getUser(@Param() param: ParamDto) {
    return this.usersService.findOneById(param.id);
  }

  @Put(':id')
  async editUser(@Param() params: ParamDto, @Body() body: EditUserDto) {
    const { roles, ...payload } = body;
    const user = await this.usersService.updateOneById(params.id, payload);
    if (!user) {
      return new NotFoundException();
    }
    if (roles) {
      await this.rolesService.updateUserRoles(user.id, roles);
    }
    return user;
  }

  @Delete(':id')
  @HttpCode(204)
  async removeUser(@Param() param: ParamDto) {
    return this.usersService.deleteOneById(param.id);
  }
}
