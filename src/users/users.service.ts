import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { USER_MODEL_NAME, UserModel, User } from './users.schema';
import { PaginateService } from '../shared/dao';

@Injectable()
export class UsersService extends PaginateService<User> {
  constructor(
    @InjectModel(USER_MODEL_NAME) private readonly userModel: UserModel,
  ) {
    super(userModel);
  }

  async findOneByUserName(username: string) {
    return this.userModel.findOne({ username });
  }

  // async create(payload: CreateUserDto): Promise<User> {
  //   const user = new this.userModel(payload);
  //   await user.save();
  //   return user.toJSON();
  // }

  // async findAll(): Promise<User[]> {
  //   const users = await this.userModel.find();
  //   return users.map(user => user.toJSON());
  // }

  // async findList(): Promise<PaginateList<User>> {
  //   const list = await this.userModel.paginate();
  //   return {
  //     data: list.docs.map(user => user.toJSON()),
  //     current: list.page,
  //     pageSize: list.limit,
  //     total: list.total,
  //   };
  // }

  // async findOneById(id: string): Promise<User> {
  //   const user = await this.userModel.findById(id);
  //   return user.toJSON();
  // }

  // async updateOneById(id: string, payload): Promise<User> {
  //   const user = await this.userModel.findByIdAndUpdate(id, payload, { new: true });
  //   return user.toJSON();
  // }

  // async deleteOneById(id: string): Promise<User> {
  //   const user = await this.userModel.findByIdAndDelete(id);
  //   return user.toJSON();
  // }
}
