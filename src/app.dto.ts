import {
  IsString,
  IsMobilePhone,
  IsOptional,
} from 'class-validator';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export class CreateReportDto {

  @ApiModelProperty()
  @IsString()
  readonly company: string;

  @ApiModelProperty()
  @IsMobilePhone('zh-CN')
  readonly phone: string;

  @ApiModelPropertyOptional()
  @IsString()
  @IsOptional()
  readonly description?: string;
}
