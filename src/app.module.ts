import { Module } from '@nestjs/common';
// import { TypeOrmModule } from '@nestjs/typeorm';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
// import * as redisStore from 'cache-manager-redis-store';
import { AppController } from './app.controller';
import { AppService } from './app.service';
// import { CatsModule } from './cats/cats.module';
import { UsersModule } from './users/users.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { SocketModule } from './socket/socket.module';
import { PostsModule } from './posts/posts.module';
import { MessagesModule } from './messages/messages.module';
import { AuthModule } from './auth/auth.module';
import { RolesModule } from './roles/roles.module';
import { PermissionsModule } from './permissions/permissions.module';
import { ScheduleModule } from './schedule/schedule.module';
import { FilesModule } from './files/files.module';

@Module({
  imports: [
    ConfigModule,
    // TypeOrmModule.forRoot({
    //   type: 'postgres',
    //   host: 'localhost',
    //   port: 5432,
    //   username: 'zhaopeidong',
    //   database: 'postgres',
    //   entities: [__dirname + '/**/*.entity{.ts,.js}'],
    //   synchronize: true,
    // }),
    // CacheModule.registerAsync({
    //   useFactory: (config: ConfigService): CacheModuleOptions => ({
    //     store: redisStore,
    //     host: 'localhost',
    //     port: 6379,
    //   }),
    //   inject: [ConfigService],
    // }),
    MongooseModule.forRootAsync({
      useFactory: (config: ConfigService): MongooseModuleOptions => ({
        uri: config.get('MONGO_URI'),
        useNewUrlParser: true,
        useCreateIndex: true,
      }),
      inject: [ConfigService],
    }),
    // CatsModule,
    UsersModule,
    SocketModule,
    PostsModule,
    MessagesModule,
    AuthModule,
    RolesModule,
    PermissionsModule,
    ScheduleModule,
    FilesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
