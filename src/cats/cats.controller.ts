import { Controller, Get, Post, Body } from '@nestjs/common';
import { CatsService } from './cats.service';

@Controller('cats')
export class CatsController {
  constructor(
    private readonly catsService: CatsService,
  ) {}

  @Get()
  findAll() {
    return this.catsService.getAll();
  }

  @Post()
  create(@Body() body) {
    return this.catsService.insertOne(body);
  }
}
