import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Cat {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  'created_at': Date;

  @UpdateDateColumn()
  'updated_at': Date;

  @Column()
  name: string;

  @Column({
    unique: true,
  })
  code: string;

  @Column()
  sex: string;

  @Column()
  age: number;

  @Column({
    default: false,
  })
  deleted: boolean;
}
