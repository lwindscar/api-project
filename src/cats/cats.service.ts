import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cat } from './cats.entity';

@Injectable()
export class CatsService {
  constructor(
    @InjectRepository(Cat) private readonly catRepository: Repository<Cat>,
  ) {}

  async insertOne(values) {
    const cat = new Cat();
    cat.name = values.name;
    cat.code = values.code;
    cat.sex = values.sex;
    cat.age = values.age;
    return this.catRepository.save(cat);
  }

  async getAll(): Promise<Cat[]> {
    return this.catRepository.find();
  }
}
