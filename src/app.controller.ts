import { Controller, Get, Post, Body, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiImplicitFile, ApiImplicitBody } from '@nestjs/swagger';
import * as path from 'path';
import { AppService } from './app.service';
import { MulterFile, PlainObject } from './shared/types';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'template', required: true, description: 'report template' })
  // @ApiImplicitBody({ name: 'company', type: String, required: true })
  // @ApiImplicitBody({ name: 'phone', type: String, required: true })
  // @ApiImplicitBody({ name: 'description', type: String, required: true })
  @Post('/create-report')
  @UseInterceptors(FileInterceptor('template'))
  async createReport(@UploadedFile() tpl: MulterFile, @Body() body: PlainObject) {
    const tplPath = path.resolve(__dirname, '..', tpl.path);
    const reportName = await this.appService.createReport(tplPath, body);
    const reportUrl = `http://localhost:3000/uploads/${reportName}`;
    return reportUrl;
  }

  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'template', required: true, description: 'report template' })
  @Post('/word2pdf')
  @UseInterceptors(FileInterceptor('template'))
  async word2PDF(@UploadedFile() tpl: MulterFile) {
    const tplPath = path.resolve(__dirname, '..', tpl.path);
    const reportName = await this.appService.wordToPDF(tplPath);
    const reportUrl = `http://localhost:3000/uploads/${reportName}`;
    return reportUrl;
  }
}
