import { WebSocketGateway, SubscribeMessage, WebSocketServer, WsResponse } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway()
export class SocketGateway {
  @WebSocketServer() server: Server;

  @SubscribeMessage('event')
  onEvent(client: Socket, data: any): WsResponse<any> {
    // client.emit('hello', 'yyyyyyyyy----=+++')
    // this.server.emit('hello', 'xxx---------');
    return { event: 'hello', data };
  }
}
